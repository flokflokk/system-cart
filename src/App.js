import React, { Component } from 'react';
import  { BrowserRouter, Switch, Route } from 'react-router-dom'
import Navbar from './components/layout/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import productDetails from './components/Product/productDetails';
import SignIn from './components/auth/SignIn';
import SignUp from './components/auth/SignUp';
import CreateProduct from './components/Product/CreateProduct';
import cart from './components/cart/cart';
import Catalog from './components/Catalog/Catalog';
import Stock from './components/Stock/Stock';
import StockEdit from './components/Stock/StockEdit';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar/>
          <Switch>
            <Route exact path='/' component={Dashboard}/>
            <Route exact path='/catalog' component={Catalog}/>
            <Route exact path='/stock/' component={Stock}/>
            <Route       path='/stock/:id/edit' component={StockEdit}/>


            <Route path='/product/:id' component={productDetails}/>
            <Route path='/signin' component={SignIn}/>
            <Route path='/signup' component={SignUp}/>
            <Route path='/create' component={CreateProduct}/>
            <Route path='/cart' component={cart}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;

import React from "react";
import { Link,NavLink } from "react-router-dom";
import SignedInLinks from "./SignedInLinks";
import SignedOutLinks from "./SignedOutLinks";
import { connect } from "react-redux";

const Navbar = props => {
  const { auth, profile } = props;
  //console.log(auth)
  const links = auth.uid ? (
    <SignedInLinks profile={profile} />
  ) : (
    <SignedOutLinks />
  );
  return (
    // <nav className="navbar colorBar" aria-label="main navigation" role="navigation">
    //     <div className="navbar-brand">
    //         <Link to='/' className="navbar-item">OSRAM</Link>
    //         { links }
    //     </div>
    // </nav>
    <nav
      className="navbar is-white "
      aria-label="main navigation"
      role="navigation"
    >
      <div className="navbar-brand">
        <a className="navbar-item" href="#" target="_blank">
        <img
            className="imgiconLogin"
            src="http://www.esa.in.th/img/esa.png"
            width="112px"
            height="28px"
          />
        </a> 
      </div>
      {links}
  </nav>
  );
};

const mapStateToProps = state => {
  console.log(state);
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile
  };
};

export default connect(mapStateToProps)(Navbar);

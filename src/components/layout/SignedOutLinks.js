import React from "react";
import { Link } from "react-router-dom";

const SignedOutLink = () => {
  return (
    // <ul className="right">
    //     <li><NavLink to='/signup'>Signup</NavLink></li>
    //     <li><NavLink to='/signin'>Login</NavLink></li>
    // </ul>
    <div id="navbarExampleTransparentExample" className="navbar-menu">
      <div className="navbar-end">
        <Link to="/signup" className="navbar-item">
        Signup
        </Link>
        <Link to="/signin" className="navbar-item">
        Login
        </Link>
      </div>
    </div>
  );
};

export default SignedOutLink;

import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { signOut } from "../../store/actions/authActions";

const SignedInLink = props => {

  return (

    <div id="navbarExampleTransparentExample" className="navbar-menu">
      <div className="navbar-end">
        <Link to="/" className="navbar-item">
          Home
        </Link>
        <Link to="/catalog" className="navbar-item">
          Catalog
        </Link>
        <Link to="/stock" className="navbar-item">
          Stock
        </Link>
        <Link to="/" className="navbar-item">
          History
        </Link>

        <div className="navbar-item has-dropdown is-hoverable">
        <Link className="navbar-link">
          Hello {props.profile.initials}
        </Link>
        <div className="navbar-dropdown">
          <a className="navbar-item">
            Change Password
          </a>
          <hr className="navbar-divider"/>          
          <a className="navbar-item" href="/signin" onClick={props.signOut}>
         Log Out
         </a>

        </div>
      </div>

      </div>
    </div>
  );
};

const mapDispatchToProps = dispatch => {
    
  return {
    signOut: () => dispatch(signOut())
  };
};

export default connect(
  null,
  mapDispatchToProps
)(SignedInLink);

import React from "react";
import { Link } from "react-router-dom";
import ProductCard from "./components/ProductCard";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";

// const products = [
//   {
//     id: 1,
//     name: "Test",
//     coverImage: ""
//   }
// ];                                 ต้องส่ง products ใน ProductList

const Catalog = ({ products }) => {
  console.log(products);
  return (
    <div className="product-list">
      <section className="hero is-medium is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Product Management</h1>
            <h2 className="subtitle">: )</h2>
          </div>
        </div>
      </section>
      <section className="section product-data">
        <div className="container">
          <h2 className="title">Catalog</h2>

          <div className="columns">
            <div className="column is-12">
              <Link to="/create" className="button is-info">
                Create
              </Link>
              <Link to="/test" className="button is-info">
                Test Form
              </Link>
            </div>
          </div>
          {products &&
            products.map(product => {
              return (
                // <Link to={"/product/" + product.id} key={product.id}>
                <ProductCard product={product} />
                //</Link>
              );
            })}
        </div>
      </section>
    </div>
  );
};

const mapStateToProps = state => {
  console.log(state);
  return {
    products: state.firestore.ordered.products,
    auth: state.firebase.auth
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: "products", orderBy: ["createAt"] }
    // ชื่อต้องตรงกับ index.js ของ firebase functions
  ])
)(Catalog);

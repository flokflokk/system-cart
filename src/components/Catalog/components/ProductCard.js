import React, { Component } from "react";
import { connect } from 'react-redux'
import '../../../css/ProductCard.css'
import { addToCart } from "../../../store/actions/cart/cartActions";
class ProductCard extends Component{
  handleClick = (id,product) => {
    // console.log("ID :",product)
    // console.log("this.props",this.props)
    this.props.addToCart(id,product);
  }
render(){
  const {product} = this.props;
  return (
    <div className="columns">
      <div className="card">
        <div className="columns ">
          <img className="imgP" src={product.imageUrl} alt={product.name} />
          <div className="content column is-one-quarter">
            <strong>Product Feature</strong>
            <p>{product.pro_feature}</p>
          </div>
          <div className="content column is-one-quarter">
            <strong>Product Benefit</strong>
            <p>{product.pro_benefit}</p>
            <a className="button is-info" > Select </a>
          </div>
          <div className="content column ">
          <button className="button is-info" onClick={()=>{this.handleClick(product.id,product)}} >
              Cart
            </button>
          </div>
          <div className="content column quantity ">
            <p>{product.price} B </p><strong>Quatinty</strong>
            <input type="number" name="quantity" min="1" max="99" /><p>{product.quantity} piece </p>
          </div>
        </div>
      </div>
    </div>
  );}};
const mapDispatchToProps= (dispatch)=>{ 
  return{
    addToCart: (id,product)=>{dispatch(addToCart(id,product))}
  }
}
export default connect(null,mapDispatchToProps)(ProductCard);

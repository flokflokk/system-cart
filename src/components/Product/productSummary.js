import React from "react";
import moment from "moment";

const ProductSummary = ({ product }) => {
    const handleClick = id => {
        this.props.addToCart(id);
      };
  return (
    <div className="card z-depth-0 project-summary">
      <div className="card-content grey-text text-darken-3">
        <span className="card-title">{product.pro_name}</span>
        <p>
          Send by {product.authorFirstName} {product.authorLastName}
        </p>
        <span
          to="/"
          className="btn-floating halfway-fab waves-effect waves-light red"
          onClick={() => {
            handleClick(product.id);
          }}
        >
          <i className="material-icons">add</i>
        </span>
        {/* <p className="grey-text">{moment(product.createAt.toDate()).calendar()}</p> */}
      </div>
    </div>
  );
}

export default ProductSummary;


import React from 'react'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Redirect } from 'react-router-dom'
import moment from 'moment'

const productDetails = (props) => {
    //console.log(props)
    const { product, auth } = props;
    if (!auth.uid) return <Redirect to='/signin' />

    if (product) {
        return (
            <div className="container section project-detail">
                <div className="card z-depth-0">
                    <div className="card-content">
                        <span className="card-title">{product.pro_name}</span>
                    </div>

                    <div className="card-content">
                        <h6>Product Infomation</h6>
                        <div className="card-stacked">
                            <p className="blue-text">{product.pro_benefit}</p>
                            <p className="blue-text">{product.pro_feature}</p>
                        </div>
                    </div>

                    <div className="card-content">
                        <h6>ที่อยู่</h6>
                        <div className="card-stacked">
                            <div className="card-content">
                                <p>{product.road}</p>
                                <p>{product.subDistrict}</p>

                            </div>
                        </div>
                    </div>

                    <div className="card-action gret lighten-4 grey-text">
                        <div>Send by {product.authorFirstName} {product.authorLastName}</div>
                        <div>{moment(product.createAt.toDate()).calendar()}</div>
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            //หน้าโหลด
            <div className="container center">
                <div className="preloader-wrapper active">
                    <div className="spinner-layer spinner-red-only">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div><div className="gap-patch">
                            <div className="circle"></div>
                        </div><div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id
    const products = state.firestore.data.products
    const product = products ? products[id] : null
    console.log("state",state)
    console.log("ownProps",ownProps)
    return {
        product: product,
        auth: state.firebase.auth
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'products' }
    ])
)(productDetails)
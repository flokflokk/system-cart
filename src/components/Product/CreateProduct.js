import React, { Component } from "react";
import { connect } from "react-redux";
import { createProduct } from "../../store/actions/productActions";
import { Redirect } from "react-router-dom";
import DropzoneUploader from "../DropzoneUploader";

export class CreateProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pro_name: "",
      brand: "",
      pro_feature: "",
      pro_benefit: "",
      price: "",
      warranty_period: "",
      quantity:"",
      imageUrl: props.coverImage
      // date: new Date()
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);

    this.props.createProduct(this.state);
    //Redirect to dashboard page after create complaint
    this.props.history.push("/");
  };
  handleUploadFile = value => {
    this.setState({
      imageUrl: value.imageUrl
    });
  };

  render() {
    const { imageUrl } = this.state;
    const { auth } = this.props;
    if (!auth.uid) return <Redirect to="/signin" />;

    return (
      <div className="container">
        <form className="white" onSubmit={this.handleSubmit}>
          <h5 className="grey-text text-darken-3">Create Product</h5>
          {/* Product */}
          <div className="input-field">
            <label htmlFor="pro_name">Product Name</label>
            <input type="text" id="pro_name" onChange={this.handleChange} />
          </div>

          <div className="input-field">
            <h6 htmlFor="brand">Image</h6>
            <br />
            <DropzoneUploader
              handleProgress={progress => {
                console.log("Progress", progress);
              }}
              handleCallBack={props => {
                console.log("props", props);
                this.setState({
                  imageUrl: props
                });
              }}
            />
            {imageUrl ? (
              <img
                src={imageUrl}
                className="preview-image-box"
                alt="Preview Cover"
              />
            ) : null}
          </div>

          <div className="input-field">
            <label htmlFor="brand">Brand Name</label>
            <input type="text" id="brand" onChange={this.handleChange} />
          </div>

          <div className="input-field">
            <label htmlFor="pro_feature">Product Feature</label>
            <textarea
              id="pro_feature"
              cols="20"
              rows="10"
              className="materialize-textarea"
              onChange={this.handleChange}
            />
          </div>
          <div className="input-field">
            <label htmlFor="pro_benefit">Product Benefit</label>
            <textarea
              id="pro_benefit"
              cols="20"
              rows="10"
              className="materialize-textarea"
              onChange={this.handleChange}
            />
          </div>

          <div className="input-field">
            <label htmlFor="price">Price</label>
            <input type="text" id="price" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="quantity">Quantity</label>
            <input type="text" id="quantity" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="warranty_period">Warranty Period</label>
            <input
              type="text"
              id="warranty_period"
              onChange={this.handleChange}
            />
          </div>

          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0">Create</button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createProduct: Product => dispatch(createProduct(Product))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateProduct);

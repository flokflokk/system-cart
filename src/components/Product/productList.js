import React from "react";
import ProductSummary from "./productSummary";
import { Link } from "react-router-dom";

const ProductList = ({ products }) => {
  return (
    <div className="project-list section">
      <h6>List Product</h6>
                { products && products.map( product => {
                    return (
                        <Link to={'/product/' + product.id} key={ product.id }>
                            <ProductSummary product={product} />
                        </Link>
                    )
                })}
     
    </div>
  );
};

export default ProductList;

import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { signUp } from "../../store/actions/authActions";

export class SignUp extends Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    idCard: "",
    gender: "",
    phone:"",
    address:"",
    date_birth:""
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.signUp(this.state);
    //console.log(this.state)
  };
  render() {
    const { auth, authError } = this.props;
    if (auth.uid) return <Redirect to="/" />;

    return (
      <div className="container">
        <form className="white" onSubmit={this.handleSubmit}>
          <h5 className="grey-text text-darken-3">Sign Up</h5>
          <div className="input-field">
            <label htmlFor="email">Email</label>
            <input type="email" id="email" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="password">Password</label>
            <input type="password" id="password" onChange={this.handleChange} />
          </div>

          {/* Firstname and Lastname */}
          <div className="input-field">
            <label htmlFor="firstName">First Name</label>
            <input type="text" id="firstName" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="lastName">Last Name</label>
            <input type="text" id="lastName" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <label htmlFor="idCard">ID Card</label>
            <input type="text" id="idCard" onChange={this.handleChange} />
          </div>
          <p>
            <label>
              <label htmlFor="gender">Gender</label>
              <br />
              <input
                name="group1"
                type="radio"
                id="gender"
                value="male"
                onChange={this.handleChange}
              />
              <span>Male</span>
            </label>
            <label>
              <input
                name="group1"
                type="radio"
                id="gender"
                value="female"
                onChange={this.handleChange}
              />
              <span>Female</span>
            </label>
          </p>
          <div className="input-field">
            <label htmlFor="address">Address</label>
            <textarea
              id="address"
              cols="20"
              rows="10"
              className="materialize-textarea"
              onChange={this.handleChange}
            />
          </div>
          <div className="input-field">
            <label htmlFor="phone">Phone Number</label>
            <input type="text" id="phone" onChange={this.handleChange} />
          </div>
          <div className="input-field">
            <p htmlFor="date_birth">Birthdate</p>
            <input type="date" id="date_birth" className="datepicker" onChange={this.handleChange} />
          </div>

          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0">Sign Up</button>
            <div className="red-text center">
              {authError ? <p>{authError}</p> : null}
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signUp: newUser => dispatch(signUp(newUser))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);

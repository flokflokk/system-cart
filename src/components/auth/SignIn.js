import React, { Component } from "react";
import { connect } from "react-redux";
import { signIn } from "../../store/actions/authActions";
import { Redirect } from "react-router-dom";
import "../../css/login.css"

export class SignIn extends Component {
  state = {
    email: "",
    password: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.signIn(this.state);
  };
  render() {
    const { authError, auth } = this.props;
    if (auth.uid) return <Redirect to="/" />;

    return (
      <div className="password">
        <section className="hero ">
          <div className="hero-body" />
        </section>
        <div className="container">
          <section>
            <form className="white" onSubmit={this.handleSubmit}>
              <div>
                <label className="labelLogin" htmlFor="email">Email :</label>
                <input
                  className="input cBox"
                  type="email"
                  id="email"
                  onChange={this.handleChange}
                />
              </div>

              <div>
                <label className="labelLogin">Password :</label>
                <input
                  className="input cBox"
                  id="password"
                  onChange={this.handleChange}
                />
              </div>

              {/* button */}
              <section className="container">
                <button className="button container is-success btLogin"> Login </button>
                <div className="red-text center">
                  {authError ? <p>{authError}</p> : null}
                </div>
              </section>
            </form>
          </section>
        </div>
      </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    authError: state.auth.authError,
    auth: state.firebase.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signIn: creds => dispatch(signIn(creds))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);

import React, { Component } from "react";
// import Notifications from './Notifications'
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import { Redirect } from "react-router-dom";

class Dashboard extends Component {
  render() {
    //console.log(this.props)
    const { products, auth } = this.props;

    if (!auth.uid) return <Redirect to="/signin" />;

    return (
      <div className="dashboard ">
        <section className="hero is-medium is-primary">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">OSRAM SHOP!</h1>
              <h2 className="subtitle">
                Shopping Website with React and Firebase
              </h2>
            </div>
          </div>
        </section>
        <section className="hero">
          <div className="hero-body">
            <div className="container" />
          </div>
        </section>
        <section className="section home-products">
          <div className="container">
            <div className="columns is-one-fifth ">
              <h2 className="title">Create SO</h2>
              <div classname="column">
                <div className="column ">
                  <input
                    className="input is-success "
                    type="text"
                    placeholder="Quatation ID"
                    value=""
                  />
                </div>
                <button className="button is-link is-right">Create</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    products: state.firestore.ordered.products,
    auth: state.firebase.auth
    //notifications: state.firestore.ordered.notifications
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: "products", orderBy: ["createAt"] }
    // ชื่อต้องตรงกับ index.js ของ firebase functions
    //{ collection: 'notifications', limit: 3, orderBy: ['time', 'desc'] }
  ])
)(Dashboard);

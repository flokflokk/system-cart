import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import "../../css/StockPage.css";

class Stock extends Component {
  handleClick = () => {
    //console.log("Product",products)
  };
  onHandleDelete = (id) => {
    console.log("product ID",id)
    // const isConfirm = window.confirm("Are you sure to delete?");
    // if (isConfirm) {
    //   //console.log('delete ', id)
    //   dispatch(deleteProduct(id));
    // }
  };
  render() {
    const { products, auth } = this.props;
    //console.log("products",products)

    if (!auth.uid) return <Redirect to="/signin" />;

    return (
      <div>
        <section className="section headerstock level">
          <div className="container ">
            <h2 className="title">Stock</h2>
          </div>
          <div className="level-right">
            <Link to="/stock/new" className="button is-info">
              Create
            </Link>
          </div>
        </section>
        <section className="section stock-products">
          <table className="table marginTable is-striped is-narrow is-hoverable is-fullwidth ">
            <thead>
              <tr>
                <th>Product Name</th>
                <th>Picture</th>
                <th>Brand</th>
                <th>Product Feature</th>
                <th>Product Benefit</th>
                <th>Price</th>
                <th>Quatinty</th>
                <th>Warranty Period</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody>
              {products &&
                products.map(product => {
                  const linkToEdit = `/stock/${product.id}/edit`;

                  return (
                    <tr>
                      <td>{product.pro_name}</td>
                      <td>
                        <img
                          className="ima_p"
                          src={product.imageUrl}
                          alt={product.name}
                        />
                      </td>
                      <td>{product.brand}</td>
                      <td>{product.pro_feature}</td>
                      <td>{product.pro_benefit}</td>
                      <td>{product.price}</td>
                      <td>{product.quantity}</td>
                      <td>{product.warranty_period}</td>
                      <td>
                        <Link to={linkToEdit}>
                          <button className="button is-link is-right" >
                            <img
                              className="imgicon"
                              src="https://image.flaticon.com/icons/svg/61/61456.svg"
                            />
                          </button>
                        </Link>
                        <button
                          className="button is-danger"
                          onClick={()=>{
                            this.onHandleDelete(product.id)
                          }}
                        //   onClick={() => {
                        //     //onHandleDelete(product.id, dispatch);
                        //   }}
                        >
                          <img
                            className="imgicon"
                            src="https://image.flaticon.com/icons/svg/64/64022.svg"
                          />
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    products: state.firestore.ordered.products,
    auth: state.firebase.auth
  };
};

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: "products", orderBy: ["createAt"] }
    // ชื่อต้องตรงกับ index.js ของ firebase functions
    //{ collection: 'notifications', limit: 3, orderBy: ['time', 'desc'] }
  ])
)(Stock);

export const createProduct = ( Product ) => {
    return ( dispatch, getState, { getFirestore }) => {
        // make async call to database
        const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;

        firestore.collection('products').add({
            ...Product,
            authorFirstName: profile.firstName,
            authorLastName: profile.lastName,
            authorId: authorId,
            createAt: new Date()
        }).then(() => {
            dispatch({
                type: 'CREATE_PRODUCT',
                Product
            })
        }).catch((err) => {
            dispatch({
                type: 'CREATE_PRODUCT_ERROR',
                err
            })
        })
    }
};
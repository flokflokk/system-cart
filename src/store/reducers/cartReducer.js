import {
  ADD_TO_CART,
  REMOVE_ITEM,
  SUB_QUANTITY,
  ADD_QUANTITY,
  ADD_SHIPPING
} from "../actions/cart/action-types/action-types";



const initState = {
  products: [],
  addedItem: [],
  total: 0
};

const cartReducer = (state = initState, action) => {
  if (action.type === ADD_TO_CART) {
    console.log("state >>", state);
    // console.log("productRef",productRef)
    console.log("action.id >>", action);
    
    const addedItems = state.products.find(product => product.id === action.id);

    console.log("addedItem >>", addedItems);
    //check if the action id exists in the addedItems
    const have_item = state.addedItem.find(product => action.product.id === product.id); 

    console.log("have_item >>", state.addedItem);  
    console.log("total",state.total)
    if (have_item) {
        addedItems.quantity += 1;
    //   console.log("addedItem", addedItems);
      console.log("have_item", have_item);

      return {
        ...state,
        total: state.total + addedItems.price
        
      };
      
    } else  {
      console.log("addedItem", addedItems);
      addedItems.quantity = 1;
      //calculating the total
      const newTotal = state.total + addedItems.price;

      return {
        ...state,
        addedItems: [...state.addedItem, addedItems],
        total: newTotal
      };
    }
  }
  
  return state;
};

export default cartReducer;


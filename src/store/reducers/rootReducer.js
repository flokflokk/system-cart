import authReducer from './authReducer'
import productReducer from './productReducer';
import { combineReducers } from 'redux'
import { firestoreReducer } from 'redux-firestore'
import { firebaseReducer } from 'react-redux-firebase'
import cartReducer from './cartReducer';


const rootReducer = combineReducers({
    cartReducer: cartReducer,
    auth: authReducer,
    product: productReducer,
    firestore: firestoreReducer,
    firebase: firebaseReducer
});

export default rootReducer
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import "firebase/storage";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCT-8X8JFkPmKvjk740i0ZsbuxaOZ3Iybs",
  authDomain: "testcart-fc853.firebaseapp.com",
  databaseURL: "https://testcart-fc853.firebaseio.com",
  projectId: "testcart-fc853",
  storageBucket: "testcart-fc853.appspot.com",
  messagingSenderId: "411559883536"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}
firebase.firestore().collection("products").doc()
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase)

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello, Sun");
});

// แจ้งเตือนเมื่อเพิ่ม notification ใหม่ ( ระบบแจ้งเตือน )
const createNotification = (notification => {
    return admin.firestore().collection('notifications')
        .add(notification)
        .then(doc => console.log('notification added', doc))
})

// สร้างโปรเจค เก็บชื่อสกุลของ user และเวลาการบันทึก
exports.complaintCreate = functions.firestore
    .document('complaints/{complaintId}')
    .onCreate( doc => {

        const complaint = doc.data();
        const notification = {
            content: 'Added a new complaint',
            user: `${complaint.authorFirstName} ${complaint.authorLastName}`,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        // การแจ้งเตือนว่า เพิ่มข้อมูลอะไรบ้าง
        return createNotification(notification);
})

// บันทึกข้อมูล user และเวลา
exports.userJoined = functions.auth.user()
    .onCreate(user => {

        return admin.firestore().collection('users')
            .doc(user.uid).get().then(doc => {

                const newUser = doc.data()
                const notification = {
                    content: 'Joined th party',
                    user: `${newUser.firstName} ${newUser.lastName}`,
                    time: admin.firestore.FieldValue.serverTimestamp()
                }
                
                // การแจ้งเตือนว่า เพิ่มข้อมูลอะไรบ้าง
                return createNotification(notification);
            })
})